#on local machine
helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm repo update
helm template postgresql-cluster \
    --set postgresqlPassword=password,postgresqlDatabase=cluster-database \
    --set replication.password=password \
    stable/postgresql > postgres.yaml

#on kops_aws image
kubectl apply -f postgres.yaml
kubectl exec -i pod/postgresql-cluster-postgresql-0 -- sh -c 'PGPASSWORD=password pg_dumpall -U postgres' > backup-all.sql
kubectl exec -i pod/postgresql-cluster-postgresql-0 -- sh -c 'PGPASSWORD=password pg_dump -U postgres cluster-database' > backup.sql

#on local machine
docker cp kops2:backup-all.sql .
docker cp kops2:backup.sql .
aws s3 cp backup-all.sql s3://andms-test-bucket/backup/backup-all.sql --sse AES256
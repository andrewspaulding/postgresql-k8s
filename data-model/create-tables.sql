CREATE TABLE "lifecyle" (
  "id" int4 NOT NULL,
  "stage" varchar(255) COLLATE "pg_catalog"."default",
  CONSTRAINT "lifecyle_pkey" PRIMARY KEY ("id")
);
ALTER TABLE "lifecyle" OWNER TO "postgres";
COMMENT ON COLUMN "lifecyle"."stage" IS '

Values:  draft, edit, invite_release, faf, public';

CREATE TABLE "profile" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_date" date,
  "last_login_date" date,
  "password" char(60) COLLATE "pg_catalog"."default",
  "about_description" varchar(255) COLLATE "pg_catalog"."default",
  "alias" varchar(255) COLLATE "pg_catalog"."default",
  "is_account_locked" bool,
  "is_account_disabled" bool,
  CONSTRAINT "profile_pkey" PRIMARY KEY ("id")
);
ALTER TABLE "profile" OWNER TO "postgres";

CREATE TABLE "profile_author" (
  "profile_id" int4 NOT NULL,
  "story_id" int4 NOT NULL,
  PRIMARY KEY ("profile_id", "story_id")
);

CREATE TABLE "profile_reader" (
  "profile_id" int4 NOT NULL,
  "story_id" int4 NOT NULL,
  PRIMARY KEY ("profile_id", "story_id")
);

CREATE TABLE "profile_story" (
  "id" int4 NOT NULL,
  "title" varchar(255),
  "publication_date" date,
  "last_modified_date" date,
  PRIMARY KEY ("id")
);

CREATE TABLE "story" (
  "id" int4 NOT NULL,
  "subject_type_id" int4,
  "title" varchar(255) COLLATE "pg_catalog"."default",
  "subtitle" varchar(255) COLLATE "pg_catalog"."default",
  "contributor" varchar(255) COLLATE "pg_catalog"."default",
  "editor" varchar(255) COLLATE "pg_catalog"."default",
  "dedication" varchar(255) COLLATE "pg_catalog"."default",
  "preface" varchar(255) COLLATE "pg_catalog"."default",
  "series_id" int4,
  "series_number" int4,
  "rating_score" numeric(2,1),
  "rating_count" int4,
  "public" varchar(255) COLLATE "pg_catalog"."default",
  "publication_date" date,
  "last_modified_date" date,
  "lifecyle_id" int4,
  "start_read_count" int4,
  "finish_read_count" int4,
  "word_count" int4,
  "locale_id" int4,
  CONSTRAINT "story_pkey" PRIMARY KEY ("id")
);
ALTER TABLE "story" OWNER TO "postgres";

CREATE TABLE "story_author" (
  "story_id" int4 NOT NULL,
  "author_id" int4 NOT NULL,
  CONSTRAINT "story_author_pkey" PRIMARY KEY ("author_id", "story_id")
);
ALTER TABLE "story_author" OWNER TO "postgres";

CREATE TABLE "story_body" (
  "id" int4 NOT NULL,
  "body" varchar(65535) COLLATE "pg_catalog"."default" NOT NULL,
  CONSTRAINT "story_body_pkey" PRIMARY KEY ("id")
);
ALTER TABLE "story_body" OWNER TO "postgres";

CREATE TABLE "story_profile" (
  "id" int4 NOT NULL,
  "name" varchar(255),
  "alias" varchar(255),
  PRIMARY KEY ("id")
);

CREATE TABLE "story_reader" (
  "story_id" int4 NOT NULL,
  "reader_id" int4 NOT NULL,
  CONSTRAINT "story_reader_pkey" PRIMARY KEY ("story_id", "reader_id")
);
ALTER TABLE "story_reader" OWNER TO "postgres";

CREATE TABLE "story_series" (
  "id" int4 NOT NULL,
  "title" varchar(255) COLLATE "pg_catalog"."default",
  CONSTRAINT "series_pkey" PRIMARY KEY ("id")
);
ALTER TABLE "story_series" OWNER TO "postgres";

CREATE TABLE "subject_type" (
  "id" int4 NOT NULL,
  "type_name" varchar(64) COLLATE "pg_catalog"."default",
  "parent_id" int4 NOT NULL,
  "description" varchar(255) COLLATE "pg_catalog"."default",
  CONSTRAINT "subject_type_pkey" PRIMARY KEY ("id")
);
ALTER TABLE "subject_type" OWNER TO "postgres";

CREATE TABLE "support_issue" (
  "id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "assigned_id" int4 NOT NULL,
  "status" varchar(255) COLLATE "pg_catalog"."default",
  "description" varchar(255) COLLATE "pg_catalog"."default",
  "thread" varchar(255) COLLATE "pg_catalog"."default",
  CONSTRAINT "support_issue_pkey" PRIMARY KEY ("id", "profile_id", "assigned_id")
);
ALTER TABLE "support_issue" OWNER TO "postgres";

ALTER TABLE "profile_author" ADD CONSTRAINT "fk_profile_author_profile_1" FOREIGN KEY ("profile_id") REFERENCES "profile" ("id");
ALTER TABLE "profile_author" ADD CONSTRAINT "fk_profile_author_profile_story_1" FOREIGN KEY ("story_id") REFERENCES "profile_story" ("id");
ALTER TABLE "profile_reader" ADD CONSTRAINT "fk_profile_reader_profile_1" FOREIGN KEY ("profile_id") REFERENCES "profile" ("id");
ALTER TABLE "profile_reader" ADD CONSTRAINT "fk_profile_reader_profile_story_1" FOREIGN KEY ("story_id") REFERENCES "profile_story" ("id");
ALTER TABLE "story" ADD CONSTRAINT "fk_story_lifecyle_1" FOREIGN KEY ("lifecyle_id") REFERENCES "lifecyle" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "story" ADD CONSTRAINT "fk_story_series_1" FOREIGN KEY ("series_id") REFERENCES "story_series" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "story" ADD CONSTRAINT "fk_story_subject_type_1" FOREIGN KEY ("subject_type_id") REFERENCES "subject_type" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "story_author" ADD CONSTRAINT "fk_story_author_story_1" FOREIGN KEY ("story_id") REFERENCES "story" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "story_author" ADD CONSTRAINT "fk_story_author_story_profile_1" FOREIGN KEY ("author_id") REFERENCES "story_profile" ("id");
ALTER TABLE "story_body" ADD CONSTRAINT "fk_story_body_story_1" FOREIGN KEY ("id") REFERENCES "story" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "story_reader" ADD CONSTRAINT "fk_story_reader_story_1" FOREIGN KEY ("story_id") REFERENCES "story" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "story_reader" ADD CONSTRAINT "fk_story_reader_story_profile_1" FOREIGN KEY ("reader_id") REFERENCES "story_profile" ("id");
ALTER TABLE "subject_type" ADD CONSTRAINT "fk_subject_type_subject_type_1" FOREIGN KEY ("parent_id") REFERENCES "subject_type" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;


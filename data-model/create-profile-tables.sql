CREATE TABLE "profile" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_date" date,
  "last_login_date" date,
  "password" char(60) COLLATE "pg_catalog"."default",
  "about_description" varchar(255) COLLATE "pg_catalog"."default",
  "alias" varchar(255) COLLATE "pg_catalog"."default",
  "is_account_locked" bool,
  "is_account_disabled" bool,
  CONSTRAINT "profile_pkey" PRIMARY KEY ("id")
);
ALTER TABLE "profile" OWNER TO "postgres";

INSERT INTO "profile"
VALUES (1,'Testing Tester','test@example.com',NULL,NULL,NULL,NULL,NULL,false,false),
       (2,'Test2 Tester2','test2@example.com',NULL,NULL,NULL,NULL,NULL,false,false);

CREATE TABLE "profile_author" (
  "profile_id" int4 NOT NULL,
  "story_id" int4 NOT NULL,
  CONSTRAINT "profile_author_pkey" PRIMARY KEY ("profile_id", "story_id")
);
ALTER TABLE "profile_author" OWNER TO "postgres";

INSERT INTO "profile_author"
VALUES (1,1),(2,2);

CREATE TABLE "profile_reader" (
  "profile_id" int4 NOT NULL,
  "story_id" int4 NOT NULL,
  CONSTRAINT "profile_reader_pkey" PRIMARY KEY ("profile_id", "story_id")
);
ALTER TABLE "profile_reader" OWNER TO "postgres";

INSERT INTO "profile_reader"
VALUES (1,2);

CREATE TABLE "profile_story" (
  "id" int4 NOT NULL,
  "title" varchar(255),
  "publication_date" date,
  "last_modified_date" date,
  CONSTRAINT "profile_story_pkey" PRIMARY KEY ("id")
);
ALTER TABLE "profile_story" OWNER TO "postgres";

INSERT INTO "profile_story"
VALUES (1,'Tester One Story',NULL,NULL),
       (2,'Tester Two Story',NULL,NULL);

ALTER TABLE "profile_author" ADD CONSTRAINT "fk_profile_author_profile_1" FOREIGN KEY ("profile_id") REFERENCES "profile" ("id");
ALTER TABLE "profile_author" ADD CONSTRAINT "fk_profile_author_profile_story_1" FOREIGN KEY ("story_id") REFERENCES "profile_story" ("id");
ALTER TABLE "profile_reader" ADD CONSTRAINT "fk_profile_reader_profile_1" FOREIGN KEY ("profile_id") REFERENCES "profile" ("id");
ALTER TABLE "profile_reader" ADD CONSTRAINT "fk_profile_reader_profile_story_1" FOREIGN KEY ("story_id") REFERENCES "profile_story" ("id");


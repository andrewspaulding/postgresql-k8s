#!/bin/bash

for table in `cat create-tables.sql | grep "CREATE TABLE" | cut -d' ' -f3 | sed 's/"//g'`
do
    sudo -u postgres psql -U 'postgres' -d 'postgres' -c "DROP TABLE ${table} CASCADE"
done

sudo -u postgres psql -U 'postgres' -d 'postgres' -f create-tables.sql
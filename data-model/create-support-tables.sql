CREATE TABLE "support_issue" (
  "id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "assigned_id" int4 NOT NULL,
  "status" varchar(255) COLLATE "pg_catalog"."default",
  "description" varchar(255) COLLATE "pg_catalog"."default",
  "thread" varchar(255) COLLATE "pg_catalog"."default",
  CONSTRAINT "support_issue_pkey" PRIMARY KEY ("id", "profile_id", "assigned_id")
);
ALTER TABLE "support_issue" OWNER TO "postgres";


# docker-compose up -d

# sleep 5

# docker cp populate-cache.py gutenberg:/tmp/populate-cache.py
# docker exec -it gutenberg python /tmp/populate-cache.py

# docker exec -it gutenberg pip install bcrypt

# docker cp get-profiles.py gutenberg:/tmp/get-profiles.py
# docker exec -it gutenberg python /tmp/get-profiles.py

docker cp get-stories.py gutenberg:/tmp/get-stories.py
docker exec -it gutenberg python /tmp/get-stories.py
docker cp gutenberg:/data/story-snippets.csv ../data/story-snippets.csv
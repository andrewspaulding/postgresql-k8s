#!/bin/bash

# Use this script to execute all python scripts in the correct order

cd ../../data-model

sh regenerate-tables.sh

cd ../test-data/scripts

python get-profiles.py
python get-stories.py

psql -U 'postgres' -d 'postgres' -c "\COPY story(id,title,subtitle) FROM '../data/stories.csv' DELIMITER ',' CSV HEADER;"
psql -U 'postgres' -d 'postgres' -c "\COPY profile(id,password,first_name,last_name,alias,email) FROM '../data/profiles.csv' DELIMITER ',' CSV HEADER;"
psql -U 'postgres' -d 'postgres' -c "\COPY story_author(story_id,author_id) FROM '../data/story-authors.csv' DELIMITER ',' CSV HEADER;"
psql -U 'postgres' -d 'postgres' -c "\COPY story_body(id,body) FROM '../data/story-bodies.csv' DELIMITER ',' CSV HEADER;"
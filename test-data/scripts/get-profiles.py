import re
import pickle
import bcrypt
from gutenberg.query import get_metadata

file_profiles = open('../data/profiles.csv','w')
file_profiles.write("name,username,email\n")
file_accounts = open('../data/accounts.csv','w')
file_accounts.write("username,password\n")
#create N/A author manually
file_profiles.write("No Author,na,no.author@example.com\n")

author_dict = {}
author_dict["No Author"] = 1

for i in [x for x in range(1, 101) if x != 40 and x != 89 ]:
  author_list = list(get_metadata('author', i))
  for author in author_list:
    if not author in author_dict:
      author_id = len(author_dict) + 1
      author_dict[author] = author_id
      split_name = author.split(", ")
      # password_string = "password1"
      password = u"password1"
      hashed_password = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt())
      password_string = hashed_password.decode('utf-8')

      if(len(split_name) > 1):
        first_name = re.sub('[^a-zA-Z]+', '-', split_name[1].lower()).strip("-")
        last_name = re.sub('[^a-zA-Z]+', '-', split_name[0].lower()).strip("-")
        username = first_name + "-" + last_name
        email = username + "@example.com"
        file_profiles.write(split_name[1] + " " + split_name[0] + "," + username + "," + email + "\n")
        file_accounts.write(username + "," + password_string + "\n")
      else:
        name_string = author
        username = re.sub('[^a-zA-Z]+', '-', author.lower()).strip("-")
        email = username + "@example.com"
        file_profiles.write(author + "," + username + "," + email + "\n")
        file_accounts.write(username + "," + password_string + "\n")


file_profiles.close()
file_accounts.close()

pickle.dump( author_dict, open( "../data/author-lookup.p", "wb" ) )
import pickle
import re
from gutenberg.query import get_metadata
from gutenberg.acquire import load_etext
from gutenberg.cleanup import strip_headers

file_metadata = open('../data/stories.csv','w')
file_metadata.write("title,slug,subtitle\n")

file_story_authors = open('../data/story-authors.csv','w')
file_story_authors.write("story_id,author_id\n")

file_story_bodies = open('../data/story-bodies.csv','w', newline='\n')
file_story_bodies.write("id,body\n")

file_story_snippets = open('../data/story-snippets.csv','w', newline='\n')
file_story_snippets.write("id,snippet\n")

author_dict = pickle.load( open( "../data/author-lookup.p", "rb" ) )
slug_dict = {}

inc = 1

for id in [x for x in range(1, 101) if x != 40 and x != 89 ]:
  title_list = list(get_metadata('title', id))
  if(len(title_list) > 0):
    title = title_list[0].replace("\"", "'").splitlines()
    file_metadata.write("\"" + title[0] + "\",")
    slug = re.sub('[^a-zA-Z0-9]+', '-', title[0].lower()).strip("-")
    if not slug in slug_dict: slug_dict[slug] = 1
    else:
      slug_increment = slug_dict[slug] + 1
      slug += "-" + str(slug_increment)
      slug_dict[slug] = slug_increment
    file_metadata.write(slug + ",")
    if(len(title) > 1):
      file_metadata.write("\"" + title[1] + "\"\n")
    else: file_metadata.write("\n")

  author_list = list(get_metadata('author', id))
  if(len(author_list) > 0):
    for author in author_list:
      if author in author_dict:
        author_id = author_dict[author]
        file_story_authors.write(str(inc) + "," + str(author_id) + "\n")
  else : file_story_authors.write(str(inc) + ",1\n") # 1 is the N/A author
  text = strip_headers(load_etext(id)).strip()
  transform_text = text.replace("\"", "'").replace("“", "'").replace("”", "'").replace('\r\n', '\n').encode('ascii', 'ignore').decode("utf-8")
  transform_text2 = transform_text.encode("unicode_escape").decode("utf-8")
  shorten_text = transform_text2[:64000]
  delta_text = "{\"\"ops\"\":[{\"\"insert\"\":\"\"" + shorten_text + "\"\"}]}"

  file_story_bodies.write(str(inc) + ",\"" + delta_text + "\"\n")

  snippet_text = transform_text2[500:716].replace("\\n", " ")
  snippet_text2 = snippet_text[snippet_text.index(' '):]
  snippet_text3 = snippet_text2.strip()
  snippet_text4 = ' '.join(snippet_text3.split())

  file_story_snippets.write(str(inc) + ",\"" + snippet_text4 + "\"\n")
  inc += 1

file_metadata.close()
file_story_authors.close()
file_story_bodies.close()
file_story_snippets.close()